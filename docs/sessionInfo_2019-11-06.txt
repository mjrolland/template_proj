R version 3.6.1 (2019-07-05)
Platform: x86_64-w64-mingw32/x64 (64-bit)
Running under: Windows 7 x64 (build 7601) Service Pack 1

Matrix products: default

locale:
[1] LC_COLLATE=English_United Kingdom.1252  LC_CTYPE=English_United Kingdom.1252    LC_MONETARY=English_United Kingdom.1252
[4] LC_NUMERIC=C                            LC_TIME=English_United Kingdom.1252    

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] tidyselect_0.2.5 fs_1.3.1         here_0.1         broom_0.5.2      mice_3.6.0       lattice_0.20-38  drake_7.7.0     
 [8] forcats_0.4.0    stringr_1.4.0    dplyr_0.8.3      purrr_0.3.3      readr_1.3.1      tidyr_1.0.0      tibble_2.1.3    
[15] ggplot2_3.2.1    tidyverse_1.2.1  rmarkdown_1.16   questionr_0.7.0 

loaded via a namespace (and not attached):
 [1] nlme_3.1-140      lubridate_1.7.4   filelock_1.0.2    httr_1.4.1        rprojroot_1.3-2   tools_3.6.1       backports_1.1.5  
 [8] R6_2.4.0          rpart_4.1-15      lazyeval_0.2.2    colorspace_1.4-1  jomo_2.6-10       nnet_7.3-12       withr_2.1.2      
[15] compiler_3.6.1    cli_1.1.0         rvest_0.3.4       xml2_1.2.2        labeling_0.3      scales_1.0.0      digest_0.6.22    
[22] txtq_0.2.0        minqa_1.2.4       pkgconfig_2.0.3   htmltools_0.4.0   lme4_1.1-21       labelled_2.2.1    fastmap_1.0.1    
[29] highr_0.8         htmlwidgets_1.5.1 rlang_0.4.1       readxl_1.3.1      rstudioapi_0.10   shiny_1.4.0       visNetwork_2.0.8 
[36] generics_0.0.2    jsonlite_1.6      magrittr_1.5      Matrix_1.2-17     Rcpp_1.0.2        munsell_0.5.0     lifecycle_0.1.0  
[43] stringi_1.4.3     yaml_2.2.0        MASS_7.3-51.4     storr_1.2.1       grid_3.6.1        parallel_3.6.1    promises_1.1.0   
[50] mitml_0.3-7       crayon_1.3.4      miniUI_0.1.1.1    haven_2.1.1       splines_3.6.1     hms_0.5.2         zeallot_0.1.0    
[57] knitr_1.25        pillar_1.4.2      igraph_1.2.4.1    boot_1.3-22       base64url_1.4     pan_1.6           glue_1.3.1       
[64] packrat_0.5.0     evaluate_0.14     modelr_0.1.5      vctrs_0.2.0       nloptr_1.2.1      httpuv_1.5.2      cellranger_1.1.0 
[71] gtable_0.3.0      assertthat_0.2.1  xfun_0.10         mime_0.7          xtable_1.8-4      later_1.0.0       survival_2.44-1.1
