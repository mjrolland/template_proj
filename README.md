# Template project

This project is an attempt to create a template for statistical analyses. I have found it to be flexible and suiting to all muy analyses. It is inspired by my experience and recent research on reproducible research.

## Description

The goal is to produce a template for statistical analyses that enables the analyst to lose as little time as possible on the non-stats part of the analysis while producing a totally reproducible report.

This is strongly inspired by the research compendium (REF).

Basically there is a `makefile.R` (here a **drake** makefile but can be a master list of regular scripts) in the root folder that should run all the main results starting from raw data import and save the last session info. All other code is stored in the `R/` folder, and all produced docs and associated rmd are stored in the `docs/` folder. Figures are saved in the `figs/` folder.

Data is not stored online for proprietary issues and a `data/` folder will have to be manually created and filled with the raw data on each copy of the project. This raw data should be set to read only and never be modified after import.

### Project architecture

* File and folder organisation:

```
.
+-- data
|   \-- data_raw.csv
+-- docs
|   +-- dir_tree.png
|   +-- main_analysis.html
|   +-- main_analysis.Rmd
|   +-- project_workflow.png
|   +-- sensitivity_analysis.html
|   +-- sensitivity_analysis.Rmd
|   +-- variable_coding.html
|   \-- variable_coding.Rmd
+-- figs
|   +-- histogram.png
|   \-- scatterplot.png
+-- makefile.R
+-- R
|   +-- analysis_plan.R
|   +-- data_preparation.R
|   +-- figures.R
|   +-- models.R
|   +-- packages.R
|   \-- tables.R
+-- README.md
\-- template_proj.Rproj
```


* Project workflow:

<img src="docs/project_workflow.png" width="100%">

### Init a similar project

1. create a project in GitLab/GitHub
2. create an Rstudio project with version control and sync to GitLab/GitHub project
3. create the different folders (data, doc, R, figs)
4. create the makefile
5. init `packages.R` and `analysis_plan.R`

## To do

* Test on multiple projects to see how flexible it is
* Proabably add a `man/` folder for function documentation
* Think about the packrat integration
* Once I have read the R packages book, see how I can translate this to packages

